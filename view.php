<?php
/**
 * This class helps abstract away view logic from controller logic
 *
 * Class View
 */
class View {

    /**
     * Render the view and give with access to an array of variables
     *
     * @param $file
     * @param array $variables
     * @return string
     */
    function render($file, $variables = array()) {
        extract($variables);

        ob_start();
        include $file;
        $renderedView = ob_get_clean();

        return $renderedView;
    }
}