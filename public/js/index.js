$(function(){

    var counter = 0;
    checkLoaded();

    // Reset page numbers when submitting form
    $('#flickrForm').keypress(function(e) {
        if(e.which == 13) {
            $('#page').val(1);
            $('#flickrForm').submit();

        }
    });

    // Reset page numbers when submitting form
    $('.container').on('click', '#submitForm', function(e) {
        e.preventDefault();
        $('#page').val(1);
        $('#flickrForm').submit();
    });

    // Grab more photos when requested with the button
    $('.container').on('click', '#morePhotos', function(e) {
        e.preventDefault();
        $('#incomingContent').html('<img src="/images/ajax-loader.gif">');

        // Get the photos
        $.ajax({
            type: 'POST',
            data: $('#flickrForm').serializeArray(),
            url: '/photos.php',
            success: function(data) {
                $('#incomingContent').before(data);
                checkLoaded();
                var currentPageNumber = parseInt($('#page').val());
                $('#page').val(currentPageNumber + 1);
            },
            dataType: 'html'
        });

        $('html, body').animate({
            scrollTop: $(document).height()
        }, 1000);
    });

    // Submit the form with different page parameter
    $('.container').on('click', '.pageNumberLink', function(e) {
        e.preventDefault();
        $('#page').val($(this).data('page-number'));
        $('#flickrForm').submit();
    });

    // Goes through the incoming images and check if they are loaded
    function checkLoaded()
    {
        counter = $('.flickrImg').length;

        $('img.flickrImg').each(function() {
            if( this.complete ) {
                imageLoaded.call( this );
            } else {
                $(this).on('load', imageLoaded);
            }
        });
    }

    // Runs after each image load
    function imageLoaded()
    {
        counter--;
        if (counter == 0) {
            allImagesLoaded();
        }
    }

    // Show images when all incoming images are loaded
    function allImagesLoaded()
    {
        $('.hide').fadeIn('slow');
        $('.hide').removeClass('hide');
        $('.flickrImg').removeClass('flickrImg');
        $('#incomingContent').html('');
    }

});