<?php

require '../services/flickr.php';
require '../services/form.php';
require '../services/paginator.php';
require '../view.php';

// Some default values and objects
$view = new View();
$form = new Form();
$customPaginator = new CustomPaginator();
$searchText = '';
$page = 1;

// The default view prior to any searching
if ( ! $_POST) {
    echo $view->render('views/index.phtml', array(
        'photos' => null,
        'searchText' => $searchText,
        'page' => $page,
    ));
    die();
}

// Perform some simple validation
if ( ! $form->validateSearch($_POST)) {
    die('form not valid');
} else {
    $searchText = $_POST['searchText'];
    $page = $_POST['page'];
}

// Get the results from the posted search string
$flickrApi = new Flickr();
$flickrSearchUrl = $flickrApi->getSearchString(array(
    'text' => $_POST['searchText'],
    'page' => $_POST['page'],
));
$apiResults = simplexml_load_file($flickrSearchUrl);

// Fields required for producing the custom pagination
$numPages = $apiResults->photos->attributes()['pages'];
$minAndMax = $customPaginator->minAndMax($page, $numPages);

// Render the view with the paginator and photo templates
echo $view->render('views/index.phtml', array(
    'searchText' => $searchText,
    'page' => $page + 1,
    'photos' => $apiResults->photos->photo,
    'photoTemplate' => $view->render('views/photos.phtml', array(
        'photos' => $apiResults->photos->photo,
    )),
    'paginator' => $view->render('views/paginator.phtml', array(
        'page' => $page,
        'minPage' => $minAndMax['minPage'],
        'maxPage' => $minAndMax['maxPage'],
        'numPages' => $numPages,
    )),
));

