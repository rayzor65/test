<?php

require '../services/flickr.php';
require '../view.php';

$flickrApi = new Flickr();
$flickrSearchUrl = $flickrApi->getSearchString(array(
    'text' => $_POST['searchText'],
    'page' => $_POST['page']
));
$apiResults = simplexml_load_file($flickrSearchUrl);


$view = new View();
echo $view->render('views/photos.phtml', array('photos' => $apiResults->photos->photo));