<?php
/**
 * For creating a paginator
 *
 * Class CustomPaginator
 */
class CustomPaginator {

    /**
     * Returns the min and max pages of the paginator
     *
     * @param $page
     * @param $numPages
     * @return array
     */
    public function minAndMax($page, $numPages) {

        $minPage = 1;
        $maxPage = $numPages;
        if ($page - 2 > 0) {
            $minPage = $page - 2;
        }

        if ($page + 2 <= $numPages) {
            $maxPage = $page + 2;
        }

        // We always want to display 5 page buttons if there are 5 pages
        if (in_array($minPage, array(1, 2)) && $numPages > 4) {
            $maxPage = $minPage + 4;
        }

        if (in_array($maxPage, array($numPages - 1, $numPages)) && $numPages > 4) {
            $minPage = $numPages - 4;
        }

        return array(
            'minPage' => $minPage,
            'maxPage' => $maxPage,
        );
    }
}
