<?php
/**
 * Functions related to forms eg validation
 *
 * Class Form
 */
class Form {

    /**
     * Validate the search term
     *
     * @param array $searchOptions
     * @return bool
     */
    public function validateSearch(array $searchOptions) {

        $valid = true;

        if (empty($searchOptions['searchText'])) {
            $valid = false;
        }

        return $valid;
    }
}
