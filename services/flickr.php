<?php
/**
 * This class produces the URL strings necessary to communicate with the Flickr API
 *
 * Class Flickr
 */
class Flickr {

    /**
     * @var string
     */
    protected $apiKey = '885e6041ad490da49c63fcdccde4b4ce';

    /**
     * @var array
     */
    protected $searchDefaults = array(
        'method' => 'flickr.photos.search',
        'format' => 'rest',
        'text' => null,
        'sort' => 'date-posted-desc',
        'content_type' => 1,
        'extras' => 'path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o',
        'per_page' => 5,
        'page' => 1,
    );

    /**
     * @var string
     */
    protected $searchApiUrl = 'http://api.flickr.com/services/rest/?api_key=';

    /**
     * Creates an URL that will search Flickr for a photo by a search term
     *
     * @param array $searchOptions
     * @return string
     */
    public function getSearchString(array $searchOptions) {
        $options = array_merge($this->searchDefaults, $searchOptions);

        $optionsStr = '&';
        foreach($options as $k => $o) {
            $optionsStr .= $k . '=' . $o . '&';
        }
        $optionsStr = rtrim($optionsStr, '&');

        // An example call will look like below
        // http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=xxxx&format=php_serial
        return $this->searchApiUrl . $this->apiKey . $optionsStr;
    }


}
